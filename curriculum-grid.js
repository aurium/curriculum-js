/* `curriculum-grid.js` is the main piece of Curriculum.js
** A javascript lib to display a nice and interactive academic curriculum grids.
**
** (C) Copyright 2014 Colivre (Aurélio A. Heckert)
** Released under the LGPLv3
** https://www.gnu.org/licenses/lgpl.html
*/

function CurriculumGrid(config) {
  if ( !config ) config = {};
  this.metaData = config.metaData || {};
  this.disciplines = {};
  this.totSeasons = 0;
  if( config.disciplines )
    for( code in config.disciplines )
      this.addDiscipline(code, config.disciplines[code]);
  this.disciplineClick = config.disciplineClick || function(){};
  config.GUI = config.GUI || {};
  this.GUI = {
    columWidth: config.GUI.columWidth || 110,
    disciplineWidth: config.GUI.disciplineWidth || 70,
    disciplineHeight: config.GUI.disciplineHeight || 40,
    disciplineMarginV: config.GUI.disciplineMarginV || 20
  }
}

CurriculumGrid.l10n = {};

var defaultL10n = {
  headerSeasonTxt: 'Season #',
  dStatus_DONE: 'Done',
  dStatus_ALLOWED: 'Allowed',
  dStatus_PENDING_DEPENDENCY: 'Pending dependency'
};

function _(strKey, replaceRE, value) {
  var str = CurriculumGrid.l10n[strKey] || defaultL10n[strKey];
  if ( replaceRE ) {
    replaceRE = new RegExp(replaceRE); // ensure that is a regexp.
    str = str.replace(replaceRE, value);
  }
  return str;
}

CurriculumGrid.StatusDisciplineStudent = {
  DONE: 1,
  ALLOWED: 2,
  PENDING_DEPENDENCY: 3
};

function die(message) {
  throw new Error(message);
}

function Discipline(curriculum, code, config) {
  this.origConfig = config;
  this.curriculum = curriculum ||
                    die('You need to define a curriculum to the discipline.');
  this.code       = code ||
                    die('You need to define a code to the discipline.');
  this.season     = config.season ||
                    die('You need to define a season to the discipline.');
  this.name       = config.name || code;
  this.dependency = config.dependency || [];
  for ( var d,i=0; d=this.dependency[i]; i++ ) {
    d = this.curriculum.disciplines[d];
    if ( d && d.season >= this.season )
      die('The dependency "'+d.code+'" must to come before "'+this.code+'".');
  }
  this.codependenciesGroup = config.codependency || [];
  for ( var d,i=0; d=this.codependenciesGroup[i]; i++ ) {
    d = this.curriculum.disciplines[d];
    if ( d && d.season != this.season )
      die('The codependency "'+d.code+'" must to come together to "'+this.code+'".');
  }
  this.status = config.status || this.autoSetStatus();
}

Discipline.prototype.getDependencies = function() {
  var listaDeps = [];
  for( var depCodigo,i=0; depCodigo = this.dependency[i]; i++ )
    listaDeps.push( this.curriculum.disciplines[depCodigo] );
  return listaDeps;
}

Discipline.prototype.getCodependencies = function() {
  var listaCodeps = [];
  for( var codepCodigo,i=0; codepCodigo = this.codependenciesGroup[i]; i++ )
    listaCodeps.push( this.curriculum.disciplines[codepCodigo] );
  return listaCodeps;
}

Discipline.prototype.autoSetStatus = function() {
  var status = CurriculumGrid.StatusDisciplineStudent;
  var listaDeps = this.getDependencies();
  var depsCursadas = true;
  for( var dep,i=0; dep = listaDeps[i]; i++ )
    depsCursadas = depsCursadas && ( dep.status == status.DONE );
  this.status = depsCursadas ? status.ALLOWED : status.PENDING_DEPENDENCY;
  return this.status;
}

Discipline.prototype.getTextStatus = function() {
  for( status in CurriculumGrid.StatusDisciplineStudent )
    if( this.status == CurriculumGrid.StatusDisciplineStudent[status] )
      return status;
}

Discipline.prototype.getLowerTextStatus = function() {
  return this.getTextStatus().toLowerCase().replace(/_/g, '-');
}

Discipline.prototype.getLocalTextStatus = function() {
  return _('dStatus_'+this.getTextStatus());
}

CurriculumGrid.prototype.addDiscipline = function(code, config) {
  if ( this.disciplines[code] ) die('The discipline code "'+code+'" already exists.');
  this.disciplines[code] = new Discipline(this, code, config);
  this.totSeasons = Math.max( this.totSeasons, config.season );
  return this.disciplines[code];
};

CurriculumGrid.prototype.seasonDisciplines = function(num) {
  var season = [], count = 0;
  // Filter by season number:
  for ( code in this.disciplines ) {
    var discipline = this.disciplines[code];
    if ( discipline.season == num ) season.push( discipline );
  }
  // Sort by codependenciesGroup to correctly display that groups:
  season = season.sort(function(a,b) {
    var aCG = a.codependenciesGroup.join('\n');
    var bCG = b.codependenciesGroup.join('\n');
    return aCG < bCG;
  });
  // Register the discipline position on the list:
  for ( var d,i=0; d=season[i]; i++ ) d.vertPos = i;
  return season;
}

var NS = {
  html: 'http://www.w3.org/1999/xhtml',
  svg:  'http://www.w3.org/2000/svg'
};

function tag(ns, tag, attrs, content) {
  var el = document.createElementNS(ns, tag);
  for ( att in attrs ) el.setAttribute(att, attrs[att]);
  if ( content ) {
    if ( content.tagName ) el.appendChild(content);
    else el.appendChild(document.createTextNode(content));
  }
  return el;
}

function div(className, content) {
  return tag( NS.html, 'div', {'class':className}, content );
}

function h2(content) {
  return tag( NS.html, 'h2', {}, content );
}

function svg(w, h, content) {
  return tag( NS.svg, 'svg', { width:w, height:h }, content );
}

function svgPath(d) {
  return tag( NS.svg, 'path', { d: d || '' } );
}

CurriculumGrid.prototype.build = function() {
  var base = div('curriculum-grid'), defs;
  var svgEl = svg(this.totSeasons*this.GUI.columWidth, '100%',
    defs = tag( NS.svg, 'defs', {} )
  );
  defs.appendChild(
    tag( NS.svg, 'marker', {id:'path-seta', orient:'auto', style:'overflow:visible;'},
      svgPath('M -1,-1 L 1,0 L -1,1 Z')
    )
  );
  defs.appendChild(
    tag( NS.svg, 'marker', {id:'path-seta-hover', orient:'auto', style:'overflow:visible;'},
      svgPath('M -1,-1 L 1,0 L -1,1 Z')
    )
  );
  base.appendChild(svgEl);
  base.disciplines = {};
  for (var i=1; i<this.totSeasons+1; i++) {
    var seasonColumn = div('season', h2( _('headerSeasonTxt','#',i) ));
    base.appendChild(seasonColumn);
    var disciplineList = this.seasonDisciplines(i);
    for (var discipline,dPos=0; discipline=disciplineList[dPos]; dPos++) {
      var disEl = div('discipline '+discipline.getLowerTextStatus(), discipline.code);
      disEl.onclick = this.disciplineClick.bind(discipline);
      base.disciplines[discipline.code] = disEl;
      disEl.paths = [];
      this.desenhaLigacaoDependencia(discipline, base, svgEl);
      disEl.onmouseover = function() {
        for( var p,j=0; p=this.paths[j]; j++ ) {
          p.setAttribute('class', 'dis-hover');
          p.style.markerEnd = 'url(#path-seta-hover)';
          p.parentNode.appendChild(p); // because z-index do not works
        }
      };
      disEl.onmouseout = function() {
        for( var p,j=0; p=this.paths[j]; j++ ) {
          p.setAttribute('class', '');
          p.style.markerEnd = 'url(#path-seta)';
        }
      };
      var coDep = discipline.codependenciesGroup;
      if ( coDep.length > 1 ) {
        // Has a codependency group, so add to the group element.
        var group = this.getOrCreateCoDepGroupFor(coDep, seasonColumn);
        group.appendChild(disEl);
      } else {
        // Has no codependency group, so add directly to the season column.
        seasonColumn.appendChild(disEl);
      }
    }
  }
  return base;
};

CurriculumGrid.prototype.getOrCreateCoDepGroupFor = function(coDep, seasonColumn) {
  className = coDep.join('_').replace(/[^a-z0-9_]+/ig, '-');
  var group = seasonColumn.getElementsByClassName(className)[0];
  if ( !group ) {
    group = div(className+' codep');
    seasonColumn.appendChild(group);
  }
  return group;
};

Discipline.prototype.getCenter = function() {
  var gui = this.curriculum.GUI;
  var x = ((this.season-1) * gui.columWidth) + gui.columWidth/2;
  var y = (this.vertPos * (gui.disciplineHeight + gui.disciplineMarginV)) +
           gui.disciplineHeight/2 + gui.disciplineMarginV;
  return { x:x, y:y };
}

CurriculumGrid.prototype.desenhaLigacaoDependencia = function(discipline, baseEl, svgEl) {
  var listaDeps = discipline.getDependencies();
  var dis, dependency, dep, i, dW, dH, margin, inc=8, path;
  var totDeps = listaDeps.length;
  for( dependency,i=0; dependency=listaDeps[i]; i++ ) {
    dep = dependency.getCenter();
    dis = discipline.getCenter();
    dis.y = dis.y - (totDeps-1)*inc/2;
    marginW = this.GUI.columWidth - this.GUI.disciplineWidth;
    marginH = this.GUI.disciplineMarginV;
    dW = this.GUI.disciplineWidth/2;
    dH = this.GUI.disciplineHeight/2;
    if( discipline.season == dependency.season+1 ) {
      path = ' M '+(dep.x+dW)+','+dep.y +
      ' C '+(dep.x+dW+marginW*0.75)+','+dep.y +
      ' '+(dis.x-dW-marginW*0.75)+','+(dis.y+i*inc) +
      ' '+(dis.x-3-dW)+','+(dis.y+i*inc);
    } else {
      path = ' M '+(dep.x+dW)+','+dep.y +
      ' C '+(dep.x+dW+marginW*0.75)+','+dep.y +
      ' '+(dep.x+dW+marginW*0.25)+','+(dep.y+dH+marginH*0.5) +
      ' '+(dep.x+dW+marginW)+','+(dep.y+dH+marginH*0.5) +
      ' L '+(dis.x-dW-marginW)+','+(dep.y+dH+marginH*0.5) +
      ' C '+(dis.x-dW-marginW*0.25)+','+(dep.y+dH+marginH*0.5) +
      ' '+(dis.x-dW-marginW*0.75)+','+(dis.y+i*inc) +
      ' '+(dis.x-3-dW)+','+(dis.y+i*inc);
    }
    svgEl.appendChild( path = svgPath(path) );
    path.style.markerEnd = 'url(#path-seta)';
    baseEl.disciplines[discipline.code].paths.push(path);
    baseEl.disciplines[dependency.code].paths.push(path);
  }
};

